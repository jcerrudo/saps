/*
 * mma.h
 *
 *  Created on: 19 dic. 2019
 *      Author: Prototipado
 */

//#include "i2c.h"

#ifndef HARDWARE_MMA_H_
#define HARDWARE_MMA_H_


#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "i2c.h"

//#define STATUS	0x07    // Real time status
#define OUT_X_MSB           0x01    // [7:0] are 8 MSBs of 14-bit sample
#define OUT_X_LSB           0x02    // [7:2] are 6 LSBs of 14-bit sample
#define OUT_Y_MSB           0x03    // [7:0] are 8 MSBs of 14-bit sample
#define OUT_Y_LSB           0x04    // [7:2] are 6 LSBs of 14-bit sample
#define OUT_Z_MSB           0x05    // [7:0] are 8 MSBs of 14-bit sample
#define OUT_Z_LSB           0x06    // [7:2] are 6 LSBs of 14-bit sample
// Reserved 0x07-0x08
#define SYSMOD              0x0B    // Current System Mode
#define INT_SOURCE          0x0C    // Interrupt status
#define WHO_AM_I            0x0D    // Device ID (0x1A)
#define XYZ_DATA_CFG        0x0E    // HPF Data Out and Dynamic Range Settings
#define HP_FILTER_CUTOFF    0x0F    // Cutoff frequency is set to 16 Hz @ 800 Hz
#define PL_STATUS           0x10    // Landscape/Portrait orientation status
#define PL_CFG              0x11    // Landscape/Portrait configuration
#define PL_COUNT            0x12    // Landscape/Portrait debounce counter
#define PL_BF_ZCOMP         0x13    // Back-Front, Z-Lock Trip threshold
#define P_L_THS_REG         0x14    // Portrait to Landscape Trip Angle is 29�
#define FF_MT_CFG           0x15    // Freefall/Motion functional block configuration
#define FF_MT_SRC           0x16    // Freefall/Motion event source register
#define FF_MT_THS           0x17    // Freefall/Motion threshold register
#define FF_MT_COUNT         0x18    // Freefall/Motion debounce counter
// Reserved 0x19-0x1C
#define TRANSIENT_CFG       0x1D    // Transient functional block configuration
#define TRANSIENT_SRC       0x1E    // Transient event status register
#define TRANSIENT_THS       0x1F    // Transient event threshold
#define TRANSIENT_COUNT     0x20    // Transient debounce counter
#define PULSE_CFG           0x21    // ELE, Double_XYZ or Single_XYZ
#define PULSE_SRC           0x22    // EA, Double_XYZ or Single_XYZ
#define PULSE_THSX          0x23    // X pulse threshold
#define PULSE_THSY          0x24    // Y pulse threshold
#define PULSE_THSZ          0x25    // Z pulse threshold
#define PULSE_TMLT          0x26    // Time limit for pulse
#define PULSE_LTCY          0x27    // Latency time for 2nd pulse
#define PULSE_WIND          0x28    // Window time for 2nd pulse
#define ASLP_COUNT          0x29    // Counter setting for Auto-SLEEP
#define CTRL_REG1           0x2A    // Data Rate, ACTIVE Mode
#define CTRL_REG2           0x2B    // Sleep Enable, OS Modes, RST, ST
#define CTRL_REG3           0x2C    // Wake from Sleep, IPOL, PP_OD
#define CTRL_REG4           0x2D    // Interrupt enable register
#define CTRL_REG5           0x2E    // Interrupt pin (INT1/INT2) map
#define OFF_X               0x2F    // X-axis offset adjust
#define OFF_Y               0x30    // Y-axis offset adjust
#define OFF_Z               0x31    // Z-axis offset adjust
#define MMA8451_ADDRESS     0x1C

typedef enum {
    MMA_RANGE_2G = 0,
    MMA_RANGE_4G,
    MMA_RANGE_8G
} mma8451_range_t;

typedef enum {
    MMA_STANDBY = 0,
    MMA_WAKE,
    MMA_SLEEP
} mma8451_mode_t;

// See table on page 23 in datasheet (http://www.freescale.com/files/sensors/doc/data_sheet/MMA8452Q.pdf)
typedef enum {
    MMA_HP1 = 0,
    MMA_HP2,
    MMA_HP3,
    MMA_HP4
} mma8451_highpass_mode_t;

typedef enum {
    MMA_PORTRAIT_UP = 0,
    MMA_PORTRAIT_DOWN,
    MMA_LANDSCAPE_RIGHT,
    MMA_LANDSCAPE_LEFT
} mma8451_orientation_t;

typedef enum {
    MMA_FREEFALL = 0,
    MMA_MOTION
} mma8451_motion_type_t;

// sleep sampling mode
typedef enum {
    MMA_SLEEP_50hz = 0,
    MMA_SLEEP_12_5hz,
    MMA_SLEEP_6_25hz,
    MMA_SLEEP_1_56hz
} mma8451_sleep_frequency_t;

// normal running mode
typedef enum {
    MMA_800hz = 0,
    MMA_400hz,
    MMA_200hz,
    MMA_100hz,
    MMA_50hz,
    MMA_12_5hz,
    MMA_6_25hz,
    MMA_1_56hz
} mma_datarate_t;

// power mode
typedef enum {
    MMA_NORMAL = 0,
    MMA_LOW_NOISE_LOW_POWER,
    MMA_HIGH_RESOLUTION,
    MMA_LOW_POWER
} mma_power_mode_t;

typedef enum {
    MMA_AUTO_SLEEP = 0x80,
    MMA_TRANSIENT = 0x20,
    MMA_ORIENTATION_CHANGE = 0x10,
    MMA_TAP = 0x08,
    MMA_FREEFALL_MOTION = 0x04,
    MMA_DATA_READY = 0x01
} mma_interrupt_types_t;

typedef enum {
    MMA_X = 0x01,
    MMA_Y = 0x02,
    MMA_Z = 0x04,
    MMA_ALL_AXIS = 0x07
} mma_axis_t;

bool MMA_init(void);
void MMA_getRawData(uint16_t *x, uint16_t *y, uint16_t *z);
void MMA_getAcceleration(float *x, float *y, float *z);

mma8451_mode_t MMA_getMode();
void MMA_getInterruptEvent(bool *wakeStateChanged, bool *transient, bool *landscapePortrait, bool *tap, bool *freefallMotion, bool *dataReady);
void MMA_setRange(mma8451_range_t newRange);
mma8451_range_t MMA_getRange();
void MMA_setHighPassFilter(bool enabled, mma8451_highpass_mode_t mode);
void MMA_enableOrientationChange(bool enabled, bool clearCounterWhenInvalid);
void MMA_getPortaitLandscapeStatus(bool *orientationChanged, bool *zTiltLockoutDetected, mma8451_orientation_t *orientation, bool *back);
bool MMA_isFlat(void);
void MMA_configureLandscapePortraitDetection(bool enableDetection, uint8_t debounceCount, bool debounceTimeout);
void MMA_setMotionDetectionMode(mma8451_motion_type_t motion, uint8_t axis, bool latchMotion);
void MMA_setMotionTreshold(uint8_t threshold, uint8_t debounceCount, bool resetDebounceOnNoMotion);
bool MMA_motionDetected(bool *x, bool *y, bool *z, bool *negativeX, bool *negativeY, bool *negativeZ);
void MMA_setTransientDetection(uint8_t axis, bool latchMotion, bool bypassHighPass);
bool MMA_transientDetected(bool *x, bool *y, bool *z, bool *negativeX, bool *negativeY, bool *negativeZ);
void MMA_setTransientTreshold(uint8_t threshold, uint8_t debounceCount, bool resetDebounceOnNoMotion);
void MMA_enableSingleTapDetector(uint8_t axis, bool latch);
void MMA_enableDoubleTapDetector(uint8_t axis, uint8_t minDuration, uint8_t maxDuration, bool latch, bool abortOnQuickDoubleTap);
bool MMA_getTapDetails(bool *singleTap, bool *doubleTap, bool *x, bool *y, bool *z, bool *negativeX, bool *negativeY, bool *negativeZ);
void MMA_setTapThreshold(uint8_t x, uint8_t y, uint8_t z);
void MMA_setMaxTapDuration(uint8_t maxDuration);
void MMA_setAutoSleep(bool enabled, uint8_t time, mma8451_sleep_frequency_t sleepFrequencySampling, mma_power_mode_t sleepPowerMode);
void MMA_setWakeOnInterrupt(bool transient, bool landscapePortraitChange, bool tap, bool freefall_motion);
void MMA_setDataRate(mma_datarate_t dataRate);
void MMA_setLowNoiseMode(bool enabled);
void MMA_set8BitMode(bool enabled);
void MMA_reset(void);
void MMA_setPowerMode(mma_power_mode_t powerMode);
void MMA_configureInterrupts(bool activeHigh, bool openDrain);
void MMA_setInterruptsEnabled(uint8_t interruptMask);
void MMA_setInterruptPins(bool autoSleepWake, bool transient, bool landscapePortraitChange, bool tap, bool freefall_motion, bool dataReady);
void MMA_setOffsets(int8_t x, int8_t y, int8_t z);
void MMA_setActive(bool newActive);
void MMA_standby(bool standby);
mma_datarate_t MMA_getDataRate();
void write(uint8_t reg, uint8_t value);
uint8_t read(uint8_t reg);

#endif /* HARDWARE_MMA_H_ */
