//define a IIR SOS CMSIS-DSP coefficient array

#include <stdint.h>
#include "arm_math.h"


#ifndef STAGES
#define STAGES 4
#endif
/*********************************************************/
/*                     IIR SOS Filter Coefficients       */
float32_t ba_coeff[20] = { //b0,b1,b2,a1,a2,... by stage
    +7.117773e-06, +1.423555e-05, +7.117773e-06,
    +1.879441e+00, -8.862705e-01,
    +1.000000e+00, +2.000000e+00, +1.000000e+00,
    +1.930046e+00, -9.483564e-01,
    +1.000000e+00, -2.000124e+00, +1.000124e+00,
    +1.987023e+00, -9.871069e-01,
    +1.000000e+00, -1.999876e+00, +9.998765e-01,
    +1.997552e+00, -9.975843e-01
};
/*********************************************************/
