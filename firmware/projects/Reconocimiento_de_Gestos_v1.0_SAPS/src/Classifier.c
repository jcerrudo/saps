#include "Classifier.h"

/**
* Predict class for features vector
*/
int predict(float *x) {
    uint8_t votes[4] = { 0 };
    // tree #1
    if (x[272] <= 0.128844004124403) {
        if (x[32] <= 0.09339125454425812) {
            votes[1] += 1;
        }

        else {
            votes[2] += 1;
        }
    }

    else {
        if (x[8] <= 0.5155956298112869) {
            votes[0] += 1;
        }

        else {
            votes[3] += 1;
        }
    }

    // tree #2
    if (x[46] <= 0.8644463121891022) {
        if (x[105] <= 2.1895052939653397) {
            votes[2] += 1;
        }

        else {
            votes[0] += 1;
        }
    }

    else {
        if (x[269] <= 0.15329578146338463) {
            votes[1] += 1;
        }

        else {
            if (x[227] <= 0.811153918504715) {
                votes[3] += 1;
            }

            else {
                votes[0] += 1;
            }
        }
    }

    // tree #3
    if (x[99] <= -0.6961307600140572) {
        votes[1] += 1;
    }

    else {
        if (x[233] <= 0.27221207320690155) {
            votes[2] += 1;
        }

        else {
            if (x[56] <= 0.5650324635207653) {
                votes[3] += 1;
            }

            else {
                votes[0] += 1;
            }
        }
    }

    // tree #4
    if (x[66] <= 0.35352057218551636) {
        if (x[278] <= 0.11648289486765862) {
            votes[2] += 1;
        }

        else {
            if (x[80] <= -1.0588695704936981) {
                votes[0] += 1;
            }

            else {
                votes[3] += 1;
            }
        }
    }

    else {
        votes[1] += 1;
    }

    // tree #5
    if (x[96] <= -0.9302113056182861) {
        votes[1] += 1;
    }

    else {
        if (x[43] <= 0.8586157858371735) {
            votes[2] += 1;
        }

        else {
            if (x[269] <= 0.3306823819875717) {
                votes[3] += 1;
            }

            else {
                votes[0] += 1;
            }
        }
    }

    // tree #6
    if (x[66] <= -1.2679651975631714) {
        votes[3] += 1;
    }

    else {
        if (x[67] <= 0.981024831533432) {
            if (x[19] <= -1.185749113559723) {
                votes[1] += 1;
            }

            else {
                votes[2] += 1;
            }
        }

        else {
            votes[0] += 1;
        }
    }

    // tree #7
    if (x[39] <= 0.7045325338840485) {
        if (x[266] <= 0.23556382954120636) {
            votes[2] += 1;
        }

        else {
            if (x[149] <= 0.9274507761001587) {
                votes[3] += 1;
            }

            else {
                votes[0] += 1;
            }
        }
    }

    else {
        if (x[224] <= 0.4791402816772461) {
            votes[1] += 1;
        }

        else {
            votes[3] += 1;
        }
    }

    // tree #8
    if (x[110] <= 0.43717290461063385) {
        if (x[248] <= 0.1164926216006279) {
            votes[2] += 1;
        }

        else {
            if (x[66] <= -1.2679651975631714) {
                votes[3] += 1;
            }

            else {
                if (x[264] <= 0.013430705294013023) {
                    votes[0] += 1;
                }

                else {
                    votes[1] += 1;
                }
            }
        }
    }

    else {
        votes[1] += 1;
    }

    // tree #9
    if (x[206] <= 0.13713192008435726) {
        if (x[188] <= 0.321413591504097) {
            votes[2] += 1;
        }

        else {
            votes[3] += 1;
        }
    }

    else {
        if (x[54] <= -0.20151899382472038) {
            if (x[10] <= -2.3168296813964844) {
                votes[0] += 1;
            }

            else {
                votes[3] += 1;
            }
        }

        else {
            votes[1] += 1;
        }
    }

    // tree #10
    if (x[72] <= -0.6930082142353058) {
        votes[3] += 1;
    }

    else {
        if (x[236] <= 0.09655739739537239) {
            votes[2] += 1;
        }

        else {
            if (x[156] <= 0.05101032555103302) {
                votes[0] += 1;
            }

            else {
                if (x[200] <= 0.5976759195327759) {
                    votes[1] += 1;
                }

                else {
                    votes[3] += 1;
                }
            }
        }
    }

    // tree #11
    if (x[96] <= -0.6843056976795197) {
        votes[1] += 1;
    }

    else {
        if (x[227] <= 0.22247714549303055) {
            votes[2] += 1;
        }

        else {
            if (x[59] <= 0.35164590552449226) {
                votes[3] += 1;
            }

            else {
                votes[0] += 1;
            }
        }
    }

    // tree #12
    if (x[263] <= 0.19093967974185944) {
        if (x[75] <= 0.6006039083003998) {
            votes[2] += 1;
        }

        else {
            votes[1] += 1;
        }
    }

    else {
        if (x[20] <= 0.2695365622639656) {
            votes[0] += 1;
        }

        else {
            votes[3] += 1;
        }
    }

    // tree #13
    if (x[12] <= -0.6363471150398254) {
        votes[1] += 1;
    }

    else {
        if (x[148] <= 0.36069683730602264) {
            votes[2] += 1;
        }

        else {
            if (x[26] <= 0.3379216343164444) {
                votes[0] += 1;
            }

            else {
                votes[3] += 1;
            }
        }
    }

    // tree #14
    if (x[260] <= 0.19504033774137497) {
        if (x[20] <= 0.3751283884048462) {
            votes[1] += 1;
        }

        else {
            votes[2] += 1;
        }
    }

    else {
        if (x[2] <= 0.748222291469574) {
            votes[0] += 1;
        }

        else {
            votes[3] += 1;
        }
    }

    // tree #15
    if (x[18] <= -0.6571210324764252) {
        votes[1] += 1;
    }

    else {
        if (x[100] <= -0.6118932813405991) {
            if (x[104] <= 0.19396585505455732) {
                votes[3] += 1;
            }

            else {
                votes[0] += 1;
            }
        }

        else {
            votes[2] += 1;
        }
    }

    // tree #16
    if (x[66] <= -1.2270956635475159) {
        votes[3] += 1;
    }

    else {
        if (x[93] <= -0.33140166103839874) {
            votes[1] += 1;
        }

        else {
            if (x[102] <= 0.4766346216201782) {
                votes[2] += 1;
            }

            else {
                votes[0] += 1;
            }
        }
    }

    // tree #17
    if (x[218] <= 0.0928809680044651) {
        if (x[190] <= 0.746220052242279) {
            votes[2] += 1;
        }

        else {
            votes[0] += 1;
        }
    }

    else {
        if (x[242] <= 0.2549479156732559) {
            votes[1] += 1;
        }

        else {
            if (x[46] <= 1.8270602226257324) {
                votes[3] += 1;
            }

            else {
                votes[0] += 1;
            }
        }
    }

    // tree #18
    if (x[96] <= -0.9302113056182861) {
        votes[1] += 1;
    }

    else {
        if (x[236] <= 0.26604462414979935) {
            votes[2] += 1;
        }

        else {
            if (x[17] <= 0.30918827652931213) {
                votes[0] += 1;
            }

            else {
                votes[3] += 1;
            }
        }
    }

    // tree #19
    if (x[97] <= -0.6148645952343941) {
        if (x[66] <= 0.00911492109298706) {
            if (x[11] <= 0.45802634954452515) {
                votes[0] += 1;
            }

            else {
                votes[3] += 1;
            }
        }

        else {
            votes[1] += 1;
        }
    }

    else {
        votes[2] += 1;
    }

    // tree #20
    if (x[240] <= -0.10397129133343697) {
        if (x[103] <= -2.543131470680237) {
            votes[0] += 1;
        }

        else {
            votes[1] += 1;
        }
    }

    else {
        if (x[157] <= 0.771163385361433) {
            votes[2] += 1;
        }

        else {
            if (x[287] <= 0.32150664925575256) {
                votes[3] += 1;
            }

            else {
                votes[0] += 1;
            }
        }
    }

    // tree #21
    if (x[55] <= 0.5849325805902481) {
        votes[2] += 1;
    }

    else {
        if (x[293] <= 0.08836182206869125) {
            votes[1] += 1;
        }

        else {
            if (x[29] <= 0.3191710263490677) {
                votes[0] += 1;
            }

            else {
                votes[3] += 1;
            }
        }
    }

    // tree #22
    if (x[75] <= 0.9473721086978912) {
        if (x[59] <= 0.16877445578575134) {
            votes[3] += 1;
        }

        else {
            if (x[197] <= 0.48482276499271393) {
                votes[2] += 1;
            }

            else {
                votes[0] += 1;
            }
        }
    }

    else {
        if (x[128] <= 0.6179567128419876) {
            votes[1] += 1;
        }

        else {
            votes[0] += 1;
        }
    }

    // tree #23
    if (x[103] <= -0.28359004855155945) {
        if (x[239] <= 0.30909861624240875) {
            votes[1] += 1;
        }

        else {
            if (x[263] <= 0.3427591025829315) {
                votes[3] += 1;
            }

            else {
                votes[0] += 1;
            }
        }
    }

    else {
        votes[2] += 1;
    }

    // tree #24
    if (x[96] <= -1.256738305091858) {
        votes[1] += 1;
    }

    else {
        if (x[66] <= -1.2270956635475159) {
            votes[3] += 1;
        }

        else {
            if (x[197] <= 0.13993366062641144) {
                votes[2] += 1;
            }

            else {
                votes[0] += 1;
            }
        }
    }

    // tree #25
    if (x[200] <= 0.10029279068112373) {
        votes[2] += 1;
    }

    else {
        if (x[99] <= -0.6618940979242325) {
            votes[1] += 1;
        }

        else {
            if (x[25] <= -0.7411069273948669) {
                votes[0] += 1;
            }

            else {
                votes[3] += 1;
            }
        }
    }

    // tree #26
    if (x[15] <= -0.49814680218696594) {
        votes[1] += 1;
    }

    else {
        if (x[106] <= -0.4634154886007309) {
            if (x[248] <= 0.5649957060813904) {
                votes[3] += 1;
            }

            else {
                votes[0] += 1;
            }
        }

        else {
            votes[2] += 1;
        }
    }

    // tree #27
    if (x[68] <= -0.020093537867069244) {
        votes[3] += 1;
    }

    else {
        if (x[72] <= 0.6160440444946289) {
            if (x[269] <= 0.18458502739667892) {
                votes[2] += 1;
            }

            else {
                if (x[12] <= 0.515283390879631) {
                    votes[3] += 1;
                }

                else {
                    votes[0] += 1;
                }
            }
        }

        else {
            votes[1] += 1;
        }
    }

    // tree #28
    if (x[18] <= -0.6571210324764252) {
        votes[1] += 1;
    }

    else {
        if (x[293] <= 0.22826287150382996) {
            if (x[101] <= 0.05375083163380623) {
                votes[3] += 1;
            }

            else {
                votes[2] += 1;
            }
        }

        else {
            votes[0] += 1;
        }
    }

    // tree #29
    if (x[66] <= 0.36848537623882294) {
        if (x[30] <= 0.2407994493842125) {
            votes[2] += 1;
        }

        else {
            if (x[23] <= 0.30806631594896317) {
                votes[0] += 1;
            }

            else {
                votes[3] += 1;
            }
        }
    }

    else {
        votes[1] += 1;
    }

    // tree #30
    if (x[66] <= 0.36848537623882294) {
        if (x[112] <= -0.497563898563385) {
            if (x[185] <= 1.4394627213478088) {
                votes[3] += 1;
            }

            else {
                votes[0] += 1;
            }
        }

        else {
            votes[2] += 1;
        }
    }

    else {
        votes[1] += 1;
    }

    // return argmax of votes
    uint8_t classIdx = 0;
    float maxVotes = votes[0];

    for (uint8_t i = 1; i < 4; i++) {
        if (votes[i] > maxVotes) {
            classIdx = i;
            maxVotes = votes[i];
        }
    }

    return classIdx;
};
/**
* Predict readable class name
*/
const char* predictLabel(float *x) {
    return idxToLabel(predict(x));
};
/**
* Convert class idx to readable name
*/
const char* idxToLabel(uint8_t classIdx) {
    switch (classIdx) {
        case 0:
        return "arresto";
        case 1:
        return "locomotor";
        case 2:
        return "nada";
        case 3:
        return "revelio";
        default:
        return "Houston we have a problem";
    }
};