/** \addtogroup Template Aplicación
 ** @{ */
/** \addtogroup Template Aplicación
 ** @{ */

/*==================[inclusions]=============================================*/
#include "../inc/main.h"       /* <= own header */

#include "systemclock.h"
#include "led.h"
#include "timer.h"
#include "delay.h"
#include "gpio.h"
#include "uart.h"
#include "i2c.h"
//#include "mma.h"
#include "switch.h"
//#include "MMA7260Q.h"
#include "ADXL335.h"
#include "Classifier.h"
#include "fpu_init.h"
#include "printf.h"
#include "cyclesCounter.h"

/* Header con los coeficientes del filtro a utilizar */
#include "filtro.h"

#define ARM_MATH_CM4
#define __FPU_PRESENT 1
#include "arm_math.h"
#include "arm_const_structs.h"

LPC_USART_T* UART = LPC_USART2;
/*==================[macros and definitions]=================================*/

#define abs(a) (((a) < (0)) ? (-a) : (a))

/** @brief Luego de obtenido los coeficientes del filtro descomentar esta linea  */
#define FILTER


/** @brief Luego de obtenido el modelo de inferencia descomentar esta linea  */
#define INFERRINGML

/** @brief Acelerómetro a utilizar */
//#define MMA8451
#define ADXL335
//#define MMA7260

/** @brief Número de ejes */
#define NUM_AXES		3

/** @brief Umbral inicio de movimiento */
#define ACCEL_THRESHOLD	1.5


/** @brief Periodo refractario */
#define REFRACT_TIME		1500

/** @brief Frecuencia de muestreo */
#define SAMPLE_FREC		500


#define SAMPLE_FREC_MS (1000/SAMPLE_FREC)

/** @brief Duración del movimiento */
#define TIME_SAMPLE		3.0

/** @brief Cantidad de muestras */
#define NUM_SAMPLES 	(uint16_t)(TIME_SAMPLE*SAMPLE_FREC)


/** @brief Enumeraciones de la máquina de estado principal */
typedef enum {DATALOGGING = 0, INFERRING , RECORD, DATALOGGING_THR}state_t;
/** @brief Enumeraciones de la máquina de estado de grabación de gestos */
typedef enum {WAITING_REC = 0, END_REC, LOGGING_REC, REFRACT_REC}state_rec_t;


/*==================[internal data definition]===============================*/


/** @brief Aceleración triaxial, eje x, entero. */
uint16_t x;
/** @brief Aceleración triaxial, eje y, entero. */
uint16_t y;
/** @brief Aceleración triaxial, eje z, entero. */
uint16_t z;


/** @brief Aceleración triaxial, eje x, flotante. */
float x_G;
/** @brief Aceleración triaxial, eje y, flotante. */
float y_G;
/** @brief Aceleración triaxial, eje z, flotante. */
float z_G;

/** @brief Aceleración triaxial filtrada, eje x.  */
float x_G_filt;
/** @brief Aceleración triaxial filtrada, eje y.  */
float y_G_filt;
/** @brief Aceleración triaxial filtrada, eje z.  */
float z_G_filt;


/** @brief Estructura de configuración de la UART. */
serial_config UART_USB;
/** @brief Variable para máquina de estado principal. */
state_t state;
/** @brief Variable para máquina de estado de grabación de movimientos. */
state_rec_t record_state;
/** @brief Variable auxiliar, lleva la cuenta de los "frames" triaxiales enviados. */
uint16_t frame =0;

#ifdef INFERRINGML
float features[(int)(NUM_SAMPLES * NUM_AXES)];
#endif

#ifdef FILTER
/** @brief Instancia de la estructura de datos para filtro IIR, eje x. */
arm_biquad_cascade_df2T_instance_f32 IIR1_x;
/** @brief Instancia de la estructura de datos para filtro IIR, eje y. */
arm_biquad_cascade_df2T_instance_f32 IIR1_y;
/** @brief Instancia de la estructura de datos para filtro IIR, eje z. */
arm_biquad_cascade_df2T_instance_f32 IIR1_z;

/** @brief Arreglo para los estados del filtro del eje x (dos por cada etapa). */
float32_t IIRstate_x[2*STAGES];
/** @brief Arreglo para los estados del filtro del eje y (dos por cada etapa). */
float32_t IIRstate_y[2*STAGES];
/** @brief Arreglo para los estados del filtro del eje z (dos por cada etapa). */
float32_t IIRstate_z[2*STAGES];
#endif

/*==================[internal functions declaration]=========================*/


/** @fn void Switch1()
 * @brief Función para el servicio de interrupción de la tecla 1, activa el modo datalogging continuo.
 * @param[in] No hay parámetros
 * @return No hay parámetros
 */
void Switch1 (void)
{
	printf("\r\n Pasando a modo DATALOGGING sin THRESHOLD \r\n");
	state = DATALOGGING_THR;
	LedOff(LED_1);
	LedOff(LED_3);
	LedOff(LED_2);
	LedOn(LED_RGB_R);
}

/** @fn void Switch3()
 * @brief Función para el servicio de interrupción de la tecla 3, pasa al modo grabación.
 * @param[in] No hay parámetros
 * @return No hay parámetros
 */
void Switch3 (void)
{
	printf("\r\n Pasando a modo GRABACION \r\n");
	printf("Frecuencia de muestreo: %d Hz \r\n", SAMPLE_FREC);
	printf("Ventana de tiempo: %1.1f Seg (%d muestras)\r\n", TIME_SAMPLE, NUM_SAMPLES);
	printf("Treshold: %1.1f G \n",  ACCEL_THRESHOLD);
	printf("Periodo refractario: %d mSeg \r\n", REFRACT_TIME);
	state = RECORD;
	LedOff(LED_RGB_R);
	LedOff(LED_1);
	LedOff(LED_3);
	LedOn(LED_2);
}

/** @fn void Switch2()
 * @brief Función para el servicio de interrupción de la tecla 2, pasa al modo datalogging con threshold.
 * @param[in] No hay parámetros
 * @return No hay parámetros
 */
void Switch2 (void)
{
	printf("\r\n Pasando a modo DATALOGGING con THRESHOLD \r\n");
	state = DATALOGGING;
	LedOff(LED_RGB_R);
	LedOff(LED_3);
	LedOff(LED_2);
	LedOn(LED_1);
}


/** @fn void Switch4()
 * @brief Función para el servicio de interrupción de la tecla 4, pasa al modo inferencia.
 * @param[in] No hay parámetros
 * @return No hay parámetros
 */
void Switch4 (void)
{
	printf("\r\n Pasando a modo INFERENCIA \r\n");
	state = INFERRING;
	LedOff(LED_RGB_R);
	LedOff(LED_2);
	LedOff(LED_1);
	LedOn(LED_3);
}

/** @fn bool motionDetected()
 * @brief Función para detección de movimiento
 * @param[in] ax Valores de aceleración en X
 * @param[in] ay Valores de aceleración en Y
 * @param[in] az Valores de aceleración en Z
 * @return True o False
 */
bool motionDetected(float ax, float ay, float az) {
	float thr=0;
	arm_sqrt_f32((ax*ax) + (ay*ay) + (az*az), &thr);
    return  (thr > ACCEL_THRESHOLD);
}



/*==================[external data definition]===============================*/

/** @fn void Process()
 * @brief Función para el servicio de interrupción del Timer, adquisición y procesamiento de los datos.
 * @param[in] No hay parámetros
 * @return No hay parámetros
 */
void Process(){

	#ifdef MMA8451
	MMA_getRawData(&x,&y,&z);
	MMA_getAcceleration(&x_G,&y_G,&z_G);
	#endif
	#ifdef MMA7260
	x_G = ReadXValue();
	y_G = ReadYValue();
	z_G = ReadZValue();
	#endif
    #ifdef ADXL335

	x_G = ReadXValue();
	y_G = ReadYValue();
	z_G = ReadZValue();

	#endif
	float mag;

	#ifdef FILTER
	float mag_filt;
	cyclesCounterReset();
	arm_biquad_cascade_df2T_f32(&IIR1_x,&x_G,&x_G_filt,1);
	arm_biquad_cascade_df2T_f32(&IIR1_y,&y_G,&y_G_filt,1);
	arm_biquad_cascade_df2T_f32(&IIR1_z,&z_G,&z_G_filt,1);
	uint16_t cycles = cyclesCounterRead();
	float filter_time = cyclesCounterToUs(cycles);
	arm_sqrt_f32((x_G_filt*x_G_filt) + (y_G_filt*y_G_filt) + (z_G_filt*z_G_filt), &mag_filt);
	#else
	arm_sqrt_f32((x_G*x_G) + (y_G*y_G) + (z_G*z_G), &mag);
	#endif
	switch(state){
	/* Datalogging for training section */
		case RECORD:
			switch(record_state){
			case WAITING_REC:
				if(motionDetected(x_G,y_G,z_G)){
					record_state = LOGGING_REC;
				}
				break;
			case LOGGING_REC:
				LedToggle(LED_RGB_B);
				#ifdef FILTER
				printf("%1.2f,", x_G_filt);
				printf("%1.2f,", y_G_filt);
				printf("%1.2f,", z_G_filt);
				#else
				printf("%1.2f,", x_G);
				printf("%1.2f,", y_G);
				printf("%1.2f,", z_G);
				#endif
				frame++;
				if(frame > (NUM_SAMPLES - 2)){
					record_state = END_REC;
				}
				break;
			case END_REC:
				LedOff(LED_RGB_B);
				#ifdef FILTER
				printf("%1.2f,", x_G_filt);
				printf("%1.2f,", y_G_filt);
				printf("%1.2f\r\n", z_G_filt);
				#else
				printf("%1.2f,", x_G);
				printf("%1.2f,", y_G);
				printf("%1.2f\r\n", z_G);
				#endif
				frame = 0;
				record_state = REFRACT_REC;
				break;
			case REFRACT_REC:
				LedOn(LED_RGB_G);
				DelayMs(REFRACT_TIME);
				LedOff(LED_RGB_G);
				record_state = WAITING_REC;
				break;
			}
			break;
			case DATALOGGING_THR:
					LedToggle(LED_RGB_B);
                    #ifdef FILTER
					printf("%1.2f,", 	x_G_filt);
					printf("%1.2f,", 	y_G_filt);
					printf("%1.2f,",	z_G_filt);
					#endif
					printf("%1.2f,", 	x_G);
					printf("%1.2f,", 	y_G);
					printf("%1.2f,",	z_G);
					#ifdef FILTER
					printf("%1.2f,",	mag_filt);
					printf("%1.2f\r\n",	filter_time);
					#else
					printf("%1.2f\r\n",	mag);
					#endif
					break;
		case DATALOGGING:
			if(motionDetected(x_G,y_G,z_G)){
				LedToggle(LED_RGB_B);
				#ifdef FILTER
				printf("%1.2f,", 	x_G_filt);
				printf("%1.2f,", 	y_G_filt);
				printf("%1.2f,",	z_G_filt);
				#endif
				printf("%1.2f,", 	x_G);
				printf("%1.2f,",    y_G);
				printf("%1.2f,",	z_G);
				printf("%1.2f\r\n",	mag);
			}
			else{
				LedOff(LED_RGB_B);
			}
			break;
	/* Prediction section */
		case INFERRING:
			#ifdef INFERRINGML
			switch(record_state){
				case WAITING_REC:
					if(motionDetected(x_G,y_G,z_G)){
						record_state = LOGGING_REC;
					}
					break;
				case LOGGING_REC:
					features[frame * NUM_AXES + 0] = x_G_filt;
					features[frame * NUM_AXES + 1] = y_G_filt;
					features[frame * NUM_AXES + 2] = z_G_filt;
					frame++;
					if(frame > (NUM_SAMPLES - 1)){
						record_state = END_REC;
					}
					LedToggle(LED_RGB_B);
					break;
				case END_REC:
					cyclesCounterReset();
					stdioPrintf(UART_USB.port, "%s", predictLabel(features));
					uint16_t ml_cycles = cyclesCounterRead();
					float ml_time = cyclesCounterToUs(ml_cycles);
					printf(" - Tiempo de inferencia: %1.2f\r\n",	ml_time);
					frame = 0;
					//LedOn(LED_3);
					record_state = WAITING_REC;
					LedOff(LED_RGB_B);
					break;
			}
		#else
			printf("Falta implementar el modelo para inferencia \r\n");
		#endif
		break;

	}

}


/*==================[external functions definition]==========================*/

int main(void)
{
	/* Inicialización de perifécricos */

	SystemClockInit();
	LedsInit();

	DelayMs(250);
	LedOn(LED_RGB_R);
	LedOn(LED_3);
	LedOn(LED_2);
	LedOn(LED_1);
	DelayMs(250);
	LedOff(LED_RGB_R);
	LedOff(LED_3);
	LedOff(LED_2);
	LedOff(LED_1);
	DelayMs(250);
	LedOn(LED_RGB_R);
	LedOn(LED_3);
	LedOn(LED_2);
	LedOn(LED_1);
	DelayMs(250);
	LedOff(LED_RGB_R);
	LedOff(LED_3);
	LedOff(LED_2);
	LedOff(LED_1);

	SwitchesInit();

	state = DATALOGGING_THR;

	/* Con*/
	LedOn(LED_RGB_R);
	LedOff(LED_3);
	LedOff(LED_2);
	LedOff(LED_1);

	/* Inicializacion de módulo FPU */
	fpuInit();

	/* Configuración de UART */
	UART_USB.baud_rate = 230400;
	UART_USB.port = SERIAL_PORT_PC;
	UART_USB.pSerial = NULL;
	UartInit(&UART_USB);

	cyclesCounterConfig(EDU_CIAA_NXP_CLOCK_SPEED);

	/* Configuración Timer de muestreo y procesamiento */
	timer_config timer_configA;
	timer_configA.timer = TIMER_A;
	timer_configA.period = SAMPLE_FREC_MS;
	timer_configA.pFunc = Process;


	/* Configuración de teclas */
	SwitchActivInt(SWITCH_1, Switch1);
	SwitchActivInt(SWITCH_2, Switch2);
	SwitchActivInt(SWITCH_3, Switch3);
	SwitchActivInt(SWITCH_4, Switch4);
	printf("Inicio Firmware Tp SAPS");

	/* Inicialización de acelerómetro */
	#ifdef MMA8451
	Init_I2c(400000);
	MMA_init();
	MMA_setDataRate(MMA_800hz);
	MMA_setRange(MMA_RANGE_4G);

	#endif
	#ifdef MMA7260
	MMA7260QInit(GPIO_T_FIL0, GPIO_T_FIL2,MMA7260_RANGE_6G);
	#endif
	#ifdef ADXL335
	ADXL335Init();
	#endif

	/* Instanciación de filtros digital mediante uso de la CMSIS */
	#ifdef FILTER
	arm_biquad_cascade_df2T_init_f32(&IIR1_x, STAGES, ba_coeff, IIRstate_x);
	arm_biquad_cascade_df2T_init_f32(&IIR1_y, STAGES, ba_coeff, IIRstate_y);
	arm_biquad_cascade_df2T_init_f32(&IIR1_z, STAGES, ba_coeff, IIRstate_z);
	#endif

	/* Inicialización del Timer */
	TimerInit(&timer_configA);
	TimerStart(TIMER_A);
    while(1)
    {
    	/*Nada*/
	}
    
	return 0;
}

/*==================[end of file]============================================*/

/** @}*/
/** @}*/
/** @} doxygen end group definition */
